(function ($) {

// @todo Array syntax required;
Drupal.wysiwyg.plugins['lineclears'] = {

  /**
   * Return whether the passed node belongs to this plugin.
   */
  isNode: function(node) {
    return ($(node).is('img.wysiwyg-lineclears'));
  },

  /**
   * Execute the button.
   */
  invoke: function(data, settings, instanceId) {
    if (data.format == 'html') {
      var content = this._getPlaceholder(settings);
    }
    else {
      var content = '<br class="lineclears">';
    }
    if (typeof content != 'undefined') {
      Drupal.wysiwyg.instances[instanceId].insert(content);
    }
  },

  /**
   * Replace all <br class="clear" /> tags with images.
   */
  attach: function(content, settings, instanceId) {
    content = content.replace(/<br class="lineclears">/g, this._getPlaceholder(settings));
    console.log(content);
    return content;
  },

  /**
   * Replace images with <br class="clear" /> tags in content upon detaching editor.
   */
  detach: function(content, settings, instanceId) {
    var $content = $('<div>' + content + '</div>'); // No .outerHTML() in jQuery :(
    $.each($('img.wysiwyg-lineclears', $content), function (i, elem) {
      $('<br class="lineclears">').insertBefore(elem);
      elem.parentNode.removeChild(elem);
    });
    return $content.html();
  },

  /**
   * Helper function to return a HTML placeholder.
   */
  _getPlaceholder: function (settings) {
    return '<img src="' + settings.path + '/images/spacer.gif" alt="line breaks" title="line breaks" class="wysiwyg-lineclears drupal-content" />';
  }
};

})(jQuery);
