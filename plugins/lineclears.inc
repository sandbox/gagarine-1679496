<?php

/**
 * Implementation of hook_wysiwyg_plugin
 * 
 * Should be named {$module}_{$plugin}_plugin().
 */
function lineclears_lineclears_plugin() {
  $plugins['lineclears'] = array(
    'icon file' => 'icon.gif',
    'icon title' => t('Line clear'),
    'settings' => array(),
    'title' => t('Line clear'),
    'buttons' => array('lineclears' => t('Line Breaks')),
  );
  return $plugins;
}

